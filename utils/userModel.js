module.exports = {
  userByCityProperties: [
    'id',
    'first_name',
    'last_name',
    'email',
    'ip_address',
    'latitude',
    'longitude'
  ],
  
  userByIdProperties: [
    'id',
    'first_name',
    'last_name',
    'email',
    'ip_address',
    'latitude',
    'longitude',
    'city'
  ]
}