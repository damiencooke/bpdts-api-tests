const request = require('supertest')
const { baseURL } = require('../constants')

module.exports = (url) =>
  request(baseURL)
    .get(url)