const chai = require('chai')
const expect = chai.expect
const getRequest = require('../utils/getRequest')
const { end } = require('supertest')
const { userByCityProperties } = require('../utils/userModel')

describe('GET /users', function () {
  const userURL = '/users'

  describe('When a request is made', function () {
    it('responds with a 200', function (done) {
      getRequest(userURL)
        .expect('Content-Type', /json/)
        .expect(200, done)
    })

    it('returns an array', function (done) {
      getRequest(userURL)
        .end((err, res) => {
          expect(res.body).to.be.an('array')
          done()
        })
    })

    it('has all the correct properties on the first element', function (done) {
      getRequest(userURL)
        .end((err, res) => {
          expect(res.body[0]).to.have.all.keys(userByCityProperties)
          done()
        })
    })
  })
})