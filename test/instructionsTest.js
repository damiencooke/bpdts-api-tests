const chai = require('chai')
const expect = chai.expect
const getRequest = require('../utils/getRequest')
const { end } = require('supertest')

describe('GET /instructions', function () {

  const endpoint = '/instructions'

  it('Responds with a 200', function (done) {
    getRequest(endpoint)
      .expect('Content-Type', /json/)
      .expect(200, done())
  })
  
  it('has the property todo', function (done) {
    getRequest(endpoint)
      .end((err, res) => {
        expect(res.body).to.have.all.keys('todo')
        done()
      })
  })

  it('The response body is not empty', function (done) {
    getRequest(endpoint)
      .end((err, res) => {
        expect(res.body.todo).to.not.be.empty
        done()
      })
  })
})