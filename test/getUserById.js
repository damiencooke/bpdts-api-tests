const chai = require('chai')
const expect = chai.expect
const getRequest = require('../utils/getRequest')
const { end } = require('supertest')
const { userByIdProperties } = require('../utils/userModel')

describe('GET /user/{id}', function () {
  const userURLBuilder = (id) => `/user/${id}`

  describe('when a valid id is passed in the request', function () {
    const validIdUrl = userURLBuilder('1')
    it('responds with a 200', function (done) {
      getRequest(validIdUrl)
        .expect('Content-Type', /json/)
        .expect(200, done)
    })

    it('only returns one user', function (done) {
      getRequest(validIdUrl)
        .end((err, res) => {
          expect(res.body).to.be.an('Object')
          done()
        })
    })

    it('has all the correct properties', function (done) {
      getRequest(validIdUrl)
        .end((err, res) => {
          expect(res.body).to.have.all.keys(userByIdProperties)
          expect(res.body.id).to.deep.equal(1)
          done()
        })
    })
  })

  describe('when an invalid id is passed in the request', function () {
    const invalidIdUrl = userURLBuilder('1819827381723')
    it('responds with a 404', function (done) {
      getRequest(invalidIdUrl)
        .expect('Content-Type', /json/)
        .expect(404, done)
    })

    it('has the property "message"', function (done) {
      getRequest(invalidIdUrl)
        .end((err, res) => {
          expect(res.body).to.have.all.keys('message')
          done()
        })
    })

    it('does not have an empty response body', function (done) {
      getRequest(invalidIdUrl)
        .end((err, res) => {
          expect(res.body).to.not.be.empty
          done()
        })
    })
  })
})