const chai = require('chai')
const expect = chai.expect
const getRequest = require('../utils/getRequest')
const { end } = require('supertest')
const { userByCityProperties } = require('../utils/userModel')

describe('GET /city/{city}/users', function () {
  const cityURLBuilder = (city) => `/city/${city}/users`

  describe('when a valid city is passed in the request', function () {
    const validCityURL = cityURLBuilder('London')
    it('responds with a 200', function (done) {
      getRequest(validCityURL)
        .expect('Content-Type', /json/)
        .expect(200, done)
    })

    it('has all the correct properties for the first element', function (done) {
      getRequest(validCityURL)
        .end((err, res) => {
          expect(res.body[0]).to.have.all.keys(userByCityProperties)
          done()
        })
    })

    it('is an array', function (done) {
      getRequest(validCityURL)
        .end((err, res) => {
          expect(res.body).to.be.an('array')
          done()
        })
    })
  })

  describe('when an invalid city is passed in the request', function () {
    const invalidCityURL = cityURLBuilder('1234')
    it('Responds with a 200', function (done) {
      getRequest(invalidCityURL)
        .expect('Content-Type', /json/)
        .expect(200, done)
    })

    it('returns an empty array', function (done) {
      getRequest(invalidCityURL)
        .end((err, res) => {
          expect(res.body).to.be.empty
          done()
        })
    })
  })

  describe('when a valid city is passed in the request but no users exist for that city', function () {
    const noUsersForCityURL = cityURLBuilder('lund')
    it('Responds with a 200', function (done) {
      getRequest(noUsersForCityURL)
        .expect('Content-Type', /json/)
        .expect(200, done)
    })

    it('returns an empty array', function (done) {
      getRequest(noUsersForCityURL)
        .end((err, res) => {
          expect(res.body).to.be.empty
          done()
        })
    })
  })
})